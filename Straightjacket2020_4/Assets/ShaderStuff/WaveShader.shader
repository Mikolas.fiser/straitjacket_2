﻿Shader "Hidden/WaveShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "black" {}

		//wave
		_Speed("Speed",Range(0,10)) = 0
		_Amplitude("Amplitude",Range(0,1)) = 0
		_Craziness("Craziness",Range(0,20)) = 0

		//dirt
		_DirtTex("DirtTexture", 2D) = "black" {}
		_DirtAmount("DirtAmount",Range(0,30)) = 1
		_DirtDispl("DirtDispl",2D) = "black" {}

		//black screen edges
		_Mask("Mask",2D) = "black" {}
		_MaskAmount("MaskAmount",Range(0,1)) = 0.1
	}
		Subshader
	{
		Pass
	{
		CGPROGRAM
#pragma vertex vertex_shader
#pragma fragment pixel_shader
#pragma target 2.0

		Texture2D _MainTex;
		SamplerState my_mirror_point_sampler;

		float _Speed;
		float _Amplitude;
		float _Craziness;
		sampler2D _DirtTex;
		float _DirtAmount;
		sampler2D _Mask;
		float _MaskAmount;
		sampler2D _DirtDispl;

		float4 vertex_shader(float4 vertex:POSITION) :SV_POSITION
		{
			return UnityObjectToClipPos(vertex);
		}

		float4 pixel_shader(float4 vertex:SV_POSITION) : COLOR
		{
			vector <float,2> uv = vertex.xy / _ScreenParams.xy;

			/////////float2 disp = tex2D(_DirtDispl, uv).xy;
			/////////disp = ((disp * 2) - 1) * sin(_Time.g)*0.5;

			
			uv.y = 1.0 - uv.y;
			uv.x += cos(uv.y*_Craziness + _Time.g*_Speed)*_Amplitude*(0.5 - abs(0.5 - uv.x));//bez te podledni zavorky se musel nejak resit okraj (clamp/mirror/repeat...)
			uv.y += sin(uv.x*_Craziness + _Time.g*_Speed)*_Amplitude*(0.5 - abs(0.5 - uv.y));//s tou zavorkou to neni treba, protoze se to na krajich vlni malo a urostred hodne
			return (_MainTex.Sample(my_mirror_point_sampler, uv)*(_DirtAmount*tex2D(_DirtTex, uv)));// *(1 - _MaskAmount * tex2D(_Mask, uv).a);
		}
		ENDCG
	}
	}
}
