﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : Device {

    public Transform start;
    public Transform finish;
    private float speed = 3f;
    private Vector3 currTarget;

    private void Start()
    {
        currTarget = start.position;
    }
    public override void ToggleOnOff()
    {
        isOn = !isOn;
        if (isOn)
        {
            currTarget = start.position;
        }
        else
        {
            currTarget = finish.position;
        }
        
    }

    private void FixedUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, currTarget, speed * Time.fixedDeltaTime);
    }

}
