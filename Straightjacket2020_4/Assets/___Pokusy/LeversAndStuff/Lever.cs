﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : MonoBehaviour {

    public List<Device> connectedDevices = new List<Device>();
    public GameObject onSprite;
    public GameObject offSprite;

    private bool isOn = true;
    private bool playerNearby = false;
    public List<SpriteOutline> outlineScripts = new List<SpriteOutline>();

    private void OnTriggerEnter2D(Collider2D other)
    {
        //display icon
        if(other.CompareTag("Player"))
        {
            playerNearby = true;
            foreach (var item in outlineScripts)
            {
                item.outlineSize = 5;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        //stop displaying icon
        if (other.CompareTag("Player"))
        {
            playerNearby = false;
            foreach (var item in outlineScripts)
            {
                item.outlineSize = 0;
            }
        }
    }
    private void Update()
    {
        if (playerNearby) 
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                foreach (var connectedDevice in connectedDevices)
                {
                    connectedDevice.ToggleOnOff();
                }
                isOn = !isOn;
                if (isOn)
                {
                    onSprite.SetActive(true);
                    offSprite.SetActive(false);
                }
                else
                {
                    onSprite.SetActive(false);
                    offSprite.SetActive(true);
                }
            }
        }
    }
}
