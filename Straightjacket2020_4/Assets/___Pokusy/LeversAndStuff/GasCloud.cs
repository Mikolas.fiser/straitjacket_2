﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasCloud : MonoBehaviour {

    public List<Enemy> enemies = new List<Enemy>();
    public ParticleSystem particleSystem;
    public Collider2D thisCollider;

    public void Dissolve()
    {
        foreach (Enemy enemy in enemies)
        {
            SetEnemyActive(false, enemy);
        }
        SetShader(false,true);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("enter");
            foreach (Enemy enemy in enemies)
            {
                SetEnemyActive(true, enemy);
            }
            SetShader(true,false);
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("exit");
            foreach (Enemy enemy in enemies)
            {
                SetEnemyActive(false, enemy);
            }
        }
        SetShader(false,false);
    }

    private void SetEnemyActive(bool active, Enemy enemy)
    {
        if (enemy.myFrontView != null)
        {
            enemy.myFrontView.enabled = active;
            enemy.myFrontView.GetComponent<Renderer>().enabled = active;
        }
        enemy.GetComponent<Renderer>().enabled = active;
        enemy.gameObject.GetComponent<Collider2D>().enabled = active;
    }


    public Material shaderMat;
    private void SetShader(bool on, bool final)
    {
        if(!final) StopAllCoroutines();
        if (!on)
        {
            StartCoroutine(TransitionOut(final));
        }
        else
        {
            StartCoroutine(Transition());
        }
    }

    private float currentA = 0f;
    IEnumerator Transition()
    {
        shaderMat.SetFloat("_Speed", 0.3f);
        shaderMat.SetFloat("_Craziness", 1);
        shaderMat.SetFloat("_Amplitude", 0f);
        shaderMat.SetFloat("_DirtAmount", 1f);
        shaderMat.SetFloat("_MaskAmount", 0);
        float a = currentA;
        while (a < 0.1f)
        {
            shaderMat.SetFloat("_Amplitude", a);
            a += 0.0001f;
            currentA = a;
            yield return null;
        }

    }
    IEnumerator TransitionOut(bool final)
    {
        if(final)
        {
            particleSystem.Stop();
            thisCollider.enabled = false;
        }
        float a = currentA;
        while (a > 0f)
        {
            shaderMat.SetFloat("_Amplitude", a);
            a -= 0.0001f;
            currentA = a;
            yield return null;
        }
        shaderMat.SetFloat("_Speed", 0f);
        shaderMat.SetFloat("_Craziness", 0);
        shaderMat.SetFloat("_Amplitude", 0);
        shaderMat.SetFloat("_DirtAmount", 1f);
        shaderMat.SetFloat("_MaskAmount", 0f);
        if (final)
        {            
            
            this.gameObject.SetActive(false);
            this.enabled = false;
        }
    }
}
