﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Device : MonoBehaviour {
    public bool isOn = true;
    public abstract void ToggleOnOff();
}