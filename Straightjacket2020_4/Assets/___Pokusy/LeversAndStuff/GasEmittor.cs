﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasEmittor : MonoBehaviour
{
    public List<GasCloud> gasClouds = new List<GasCloud>();          //Ame 22/02

    private bool playerNearby;
    private int state = 0;
    private int gasclouds = 0;
    public Sprite[] topParts = new Sprite[4];
    public SpriteRenderer topPartRenderer;
    public Sprite offButton;
    public SpriteRenderer buttonRendere;

    private Player player;

    public List<SpriteOutline> outlineScripts = new List<SpriteOutline>();

    void Start()
    {
        player = FindObjectOfType<Player>();
    }

    bool hitting = false;
    private void Update()
    {
        if (playerNearby)
        {
            if (Input.GetKeyDown(KeyCode.E) && !hitting)
            {
                hitting = true;
                player.movement.PlayAttackAnim();
                StartCoroutine(CoolingDown());
                state++;
                topPartRenderer.sprite = topParts[state];
                if (state==3)
                {
                    foreach (GasCloud gasCloud in gasClouds)               //Ame 22/02
                    {
                        gasCloud.Dissolve();
                    }
                    buttonRendere.sprite = offButton;
                    GetComponent<Collider2D>().enabled = false;
                    this.enabled = false;
                }
            }
        }
    }

    IEnumerator CoolingDown()
    {
        yield return new WaitForSeconds(0.66f);
        hitting = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //display icon
        if (other.CompareTag("Player"))
        {
            foreach (var item in outlineScripts)
            {
                item.outlineSize = 5;
            }
            playerNearby = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        //stop displaying icon
        if (other.CompareTag("Player"))
        {
            foreach (var item in outlineScripts)
            {
                item.outlineSize = 0;
            }
            playerNearby = false;
        }
    }




    /*    public override void ToggleOnOff()
    {
        if (isOn)
        {
            isOn = false;
            gasSystem.Stop();
            gasArea.enabled = false;
            foreach (Enemy enemy in enemies)
            {
                SetEnemyActive(false,enemy);
            }

        }
        else
        {
            isOn = true;
            gasSystem.Play();
            gasArea.enabled = true;
        }

    }*/


}
