﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform2 : Device {

    public Transform start;
    public Transform finish;
    private float speed = 3f;
    private Vector3 currTarget;

    private void Start()
    {
        currTarget = start.position;
    }
    public override void ToggleOnOff()
    {
        isOn = !isOn;
      
    }

    private void FixedUpdate()
    {
        if (isOn)
        {
            transform.position = Vector3.MoveTowards(transform.position, currTarget, speed * Time.fixedDeltaTime);
            if ((Mathf.Abs(this.transform.position.x - currTarget.x) <= 0.1f)&&
                (Mathf.Abs(this.transform.position.y - currTarget.y) <= 0.1f))
            {
                if (currTarget == start.position) currTarget = finish.position;
                else currTarget = start.position;
            }
        }
    }

}
