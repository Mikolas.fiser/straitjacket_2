﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomOfThePit : MonoBehaviour {

    private CameraFollow2 cameraFollow;

    void Start()
    {
        cameraFollow = FindObjectOfType<CameraFollow2>();
    }

	void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            //cameraFollow.FadeOut();
            cameraFollow.enabled = false;
        }
    }
}
