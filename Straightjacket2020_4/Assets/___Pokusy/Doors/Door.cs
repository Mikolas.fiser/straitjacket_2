﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Device {

    public GameObject theDoorObject;
    public Animator animator;

    private void Start()
    {
        if (isOn)
        {
            animator.SetTrigger("open");
        }

    }

    public override void ToggleOnOff()
    {
        if(isOn)
        {
            //close the door
            //theDoorObject.SetActive(true);
            animator.SetTrigger("close");
        }
        else
        {
            //open the door
            //theDoorObject.SetActive(false);
            animator.SetTrigger("open");
        }
        isOn = !isOn;
    }

}
