﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[Serializable]
[CreateAssetMenu(fileName = "New Tile3D", menuName = "Tiles/Tile3D")]
public class Tile3D : Tile {

    

    public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
    {
        base.GetTileData(position, tilemap, ref tileData);
        /*PillManager pm = FindObjectOfType<PillManager>();
        if (pm != null)
        {
            var sws = gameObject.GetComponentsInChildren<Switcher>();
            foreach (var item in sws)
            {
                pm.AddSwitchable(item);
            }
        }*/
       /* if (Application.isEditor)
        {
            Debug.Log("playing now....");
            tileData.sprite = null;
        }*/

    }

    public override bool StartUp(Vector3Int position, ITilemap tilemap, GameObject go)
    {

        return base.StartUp(position, tilemap, go);
    }

    public override void RefreshTile(Vector3Int position, ITilemap tilemap)
    {
        base.RefreshTile(position, tilemap);
        
    }


}
