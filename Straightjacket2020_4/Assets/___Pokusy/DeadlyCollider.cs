﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadlyCollider : MonoBehaviour {

    private Player thePlayer;

    void Start()
    {
        thePlayer = FindObjectOfType<Player>();
    }

	void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            thePlayer.KillPlayer();
        }
    }
}
