﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//the lamp object
//can swing and can flicker

public class Lights : MonoBehaviour {

    Rigidbody2D myRigidbody;
    public int force = 50;
    private Animator anim;
    public Light light;

    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        if(myRigidbody!=null)//if the lamp has a rigidbody2D it will swing using the force
            myRigidbody.AddForce(new Vector2(force,0));
        anim = GetComponentInChildren<Animator>();
        light = GetComponentInChildren<Light>();
    }

    public void Flicker()
    {
        anim.SetBool("flicker", true);
    }

    public void StopFlicker()
    {
        anim.SetBool("flicker", false);
    }

}
