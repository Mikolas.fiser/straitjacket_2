﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Saving{

    public static int level = 0;
    public static Vector3 position = new Vector3(-570f, 145f, 0f);// new Vector3(16.35f, -2.47f, -2.25f);
    public static bool hasPill = false;
    public static bool[] unluckedCheckpoints = new bool[7]{ true, true, true, true, true, true, true };
}

[System.Serializable]
public class SavedData
{
    public int level = 0;
    public float posX;
    public float posY;
    public float posZ;
    public bool hasPill = false;
    public int maxUnlockedIndex = 0;
}
