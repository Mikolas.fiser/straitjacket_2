﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//for things that change sprite (hidding spots, evil TVs,...)

public class SpriteSwitcher : Switcher {

    public Sprite normalSprite;
    public Sprite crazySprite;

    public SpriteRenderer myRenderer;

    public override void Switch(bool crazy)
    {
        if (myRenderer != null)
        {
            if (crazy)
            {
                myRenderer.sprite = crazySprite;
            }
            else
            {
                myRenderer.sprite = normalSprite;
            }
        }
    }
}
