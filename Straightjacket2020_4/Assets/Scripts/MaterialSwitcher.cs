﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//for things that change material (the floor)

public class MaterialSwitcher : Switcher
{
    public Material normalMat;
    public Material crazyMat;

    public override void Switch(bool crazy)
    {
        if (crazy)
        {
            GetComponent<Renderer>().material = crazyMat;
        }
        else
        {
            GetComponent<Renderer>().material = normalMat;
        }
    }
}
