﻿using Spine;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintPatient : Switcher
{

    public Collider2D myCollider;
    public GameObject hintCanvas;
    public GameObject crazyHintCanvas;
    public string hintText;

    private Skeleton skeleton;
    private SkeletonAnimation skelAnim;
    [SpineSkin] public string normalSkin;
    [SpineSkin] public string crazySkin;
    [SpineAnimation] public string talk;
    [SpineAnimation] public string idle;
    [SpineAnimation] public string magic_idle;
    private bool normal = true;
    private bool idling = true;

    void Start()
    {
        skelAnim = GetComponent<SkeletonAnimation>();
        skeleton = skelAnim.skeleton;
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        idling = false;
        if (normal)
        {
            if(crazyHintCanvas!=null) crazyHintCanvas.SetActive(false);
            hintCanvas.SetActive(true);
            skelAnim.AnimationState.SetAnimation(0, talk, true);
        }
        else
        {
            if (crazyHintCanvas != null) crazyHintCanvas.SetActive(true);
            hintCanvas.SetActive(false);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        idling = true;
        hintCanvas.SetActive(false);
        if (crazyHintCanvas != null) crazyHintCanvas.SetActive(false);
        if (normal)
        {
            skelAnim.AnimationState.SetAnimation(0, idle, true);
        }
        else
        {
            skelAnim.AnimationState.SetAnimation(0, magic_idle, true);
        }
    }

    public override void Switch(bool crazy)
    {
        if (crazy)
        {
            skeleton.SetSkin(crazySkin);
            normal = false;
            skelAnim.AnimationState.SetAnimation(0, magic_idle, true);
            if (hintCanvas.activeSelf)
            {
                hintCanvas.SetActive(false);
                if (crazyHintCanvas != null) crazyHintCanvas.SetActive(true);
            }
        }
        else
        {
            skeleton.SetSkin(normalSkin);
            normal = true;
            if (idling) skelAnim.AnimationState.SetAnimation(0, idle, true);
            else skelAnim.AnimationState.SetAnimation(0, talk, true);
            if ((crazyHintCanvas != null) && (crazyHintCanvas.activeSelf))
            {
                hintCanvas.SetActive(true);
                if (crazyHintCanvas != null) crazyHintCanvas.SetActive(false);
            }
        }
    }
}

