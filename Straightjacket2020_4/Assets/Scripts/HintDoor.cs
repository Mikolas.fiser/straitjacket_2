﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintDoor : MonoBehaviour {

    public Collider2D myCollider;
    public GameObject hintCanvas;
    public string hintText;

    private void OnTriggerEnter2D(Collider2D other)
    {
        hintCanvas.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        hintCanvas.SetActive(false);
    }
}
