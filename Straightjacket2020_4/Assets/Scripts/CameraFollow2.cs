﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//script for the main camera
//normally it follows the player
//it allows look down
//also handles walls (that it does not look through the side walls if possible)

public class CameraFollow2 : MonoBehaviour {

    private Transform target;
    public float followSpeed = 1f;

    public float yOffset = 4.5f;//the camera starts a bit above the player
    private float depthOfViewS = 5f;//how far down can the player see
    public float z = -12f;//the z distance of the camera
    public float rotationX = 4f;

    //for looking down
    private bool Sflag = false;
    public bool canLookDownNow = false;


    //for wall stops
    private Transform closetsWall = null;
    ////public List<Transform> walls = new List<Transform>();
    private Manager mngr;
    private Transform player;
    private float criticalWallDist = 8+5f;


    //for boss room
    public bool inBossRoom = false;

    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        mngr = GameObject.Find("Manager").GetComponent<Manager>();
    }

    public SpriteRenderer fader;

    public void FadeOut()
    {
        StopAllCoroutines();
        StartCoroutine(Fade(true));
    }

    public void FadeIn()
    {
        StopAllCoroutines();
        StartCoroutine(Fade(false));
    }

    IEnumerator Fade(bool fOut)
    {
        Color col = fader.color;
        float alpha; if (fOut) alpha = 0; else alpha = 1;
        while((fOut&&alpha<1)||(0<alpha && (!fOut)))
        {
            if (fOut) alpha += Time.deltaTime; else alpha -= Time.deltaTime;
            fader.color = new Color(col.r, col.g, col.g, alpha);
            yield return null;
        }

       
    }


    public PillManager pm;
    void Update () {

       /* if (pm.onDrugs)
            followSpeed = 3f;
        else
            followSpeed = 1f;*/

        if (inBossRoom)//boss room camera
        {
            Vector3 newPos = new Vector3(603, -111.5f, -30);
            this.transform.position = Vector3.Lerp(this.transform.position, newPos, followSpeed * Time.deltaTime);
        }
        else //normal camera
        {
          /*  if (pm.onDrugs)
            {
                yOffset = 3f;
            }
            else
            {
                yOffset = 4.5f;
            }*/

            // if is pressed S the player will look down
            if (canLookDownNow && Input.GetKey(KeyCode.S) && Mathf.Abs(target.position.y - this.transform.position.y) < depthOfViewS)
            {
                Vector3 newPos = target.position;
                newPos.z = z;
                newPos.y = target.position.y - 20;
                this.transform.position = Vector3.Lerp(this.transform.position, newPos, followSpeed * Time.deltaTime);
                Sflag = true;
            }
            //check if stopped looking down
            if (Sflag == true && (Input.GetKey(KeyCode.S) == false || !canLookDownNow))
            {
                Sflag = false;
            }


            // if not looking down
            if (!Sflag)
            {
                float minDist = float.MaxValue;
                Transform minWall = null;
                //find closet wall where the player is too close to it
                foreach (Transform wall in mngr.walls)
                {
                    if (wall != null)
                    {
                        float dist = wall.position.x - player.position.x;
                        if (Mathf.Abs(dist) < criticalWallDist && (wall.position.y - player.position.y) < 6 && (wall.position.y - player.position.y) > 0)//player very close to the the wall
                        {
                            if (Mathf.Abs(dist) < Mathf.Abs(minDist))
                            {
                                minDist = dist;
                                minWall = wall;
                                break;
                            }
                        }
                    }
                }


                //if there is any such wall, the camera should not go through
                if ((minWall != null) && (player.gameObject.GetComponent<Player>().currentState != Player.State.hidden))
                {
                    //disable player following
                    //fix camera on the correct position
                    target = minWall.transform;
                    Vector3 newPos = target.position;
                    if (minDist >= 0)//if the player approached the wall from the left
                    {
                        newPos = new Vector3(newPos.x - (7), player.transform.position.y + yOffset, z);
                    }
                    else//if the player approached the wall from the right
                    {
                        newPos = new Vector3(newPos.x + (7), player.transform.position.y + yOffset, z);
                    }
                    this.transform.position = Vector3.Lerp(this.transform.position, newPos, followSpeed * Time.deltaTime);
                }
                else
                {
                    //when player left the critical distance everything should go back to normal
                    target = player;

                    //if distance of the center of the camera from the player is too big (horizontal)
                    //start followng the x coordinate of the player
                    if (!Sflag)
                    {
                        Vector3 newPos = target.position;
                        newPos.z = z;
                        newPos.y = target.position.y + yOffset;
                        if (target.gameObject.GetComponent<Player>().currentState != Player.State.hidden)
                        {

                            if (target.gameObject.GetComponent<PlayerMovement>().facingRight)
                            {
                                newPos.x += 5;
                            }
                            else
                            {
                                newPos.x -= 5;
                            }
                        }
                        this.transform.position = Vector3.Lerp(this.transform.position, newPos, followSpeed * Time.deltaTime);
                    }

                    //if distance of the center of the camera from the player is too big (vertical)
                    //start following the y coordinate of the player 
                    if (!Sflag)
                    {
                        Vector3 newPos = target.position;
                        newPos.x = transform.position.x;
                        newPos.y = target.position.y + yOffset;
                        newPos.z = z;
                        this.transform.position = Vector3.Lerp(this.transform.position, newPos, followSpeed * Time.deltaTime);
                    }
                }
            }
        }


    }
}
