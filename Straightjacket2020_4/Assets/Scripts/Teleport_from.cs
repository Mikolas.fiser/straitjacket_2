﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this will be the corridor leading to different room (different part of the level)

public class Teleport_from : MonoBehaviour
{
    private Transform pl;
    public Transform to;

    public CameraFollow2 mainCamera;

    private void Start()
    {
        pl = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        StartCoroutine(GoToNextRoom());

    }

    IEnumerator GoToNextRoom()
    {
        mainCamera.FadeOut();
        yield return new WaitForSeconds(1f);
        pl.position = to.position;
        Vector3 newPos = to.position;
        newPos.z = -12;
        mainCamera.transform.position = newPos;
        mainCamera.FadeIn();
    }
}