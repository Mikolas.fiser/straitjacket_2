﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//the script on the bottom collider of a climbing wall
//just holds references to other parts of the wall

public class ClimbingWall : MonoBehaviour {

    public Transform top;//the heihgt the player should reach during climbing
    public BoxCollider2D topc; //the area that should not be bloced by e.g. a breakable in order to be able to climbe this wall
    public Transform end;//the final position the player shoud reach after climbing
    public Transform railPoint;

    public GameObject climbSign;//UI element

    void Start()
    {
        climbSign.SetActive(false);
    }
}
