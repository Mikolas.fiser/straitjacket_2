﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//only holds reference to UI for look down spots

public class LokkDownSpot : MonoBehaviour {

    public GameObject lookDownSign;

    void Start()
    {
        lookDownSign.SetActive(false);
    }
}
