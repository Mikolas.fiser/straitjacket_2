﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//abstract class for things that change visually on world switch (wardrobes, enemies, floor,...)
//(the background is handeled differently)

public abstract class Switcher : MonoBehaviour {

    public abstract void Switch(bool crazy);
}
