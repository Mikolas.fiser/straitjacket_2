﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//the hidding spot object (a wardrobe or potted plant)
//only manages displaing the UI and switching between empty/hidden sprite

public class HiddingSpot : MonoBehaviour {

    public Sprite normal;
    public Sprite hidden;
    private SpriteRenderer myRenderer;

    public bool isBush;

    public GameObject spinningPart;

    public SpriteOutline outlineScript;

    public Animator animator;

    void Start()
    {
        myRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        
    }

    public void SetSpriteToHidden()
    {
        myRenderer.sprite = hidden;
        if (spinningPart != null)
            spinningPart.SetActive(true);
    }

    public void SetSpriteToNormal()
    {
        myRenderer.sprite = normal;
        if (spinningPart != null)
            spinningPart.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            outlineScript.outlineSize = 5;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            outlineScript.outlineSize = 0;
        }
    }
}
