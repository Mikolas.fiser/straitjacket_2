﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine;
using Spine.Unity;

//takes care of movement (reading input and actually moving the player)
//and his looping animations (idle, walk, climbe, fall)
//and has public methods which pause the looping ones and play other one-time animations (attack, hide, stunn)

public class PlayerMovement : Switcher
{

    //basic movement
    public bool facingRight = true;
    private Rigidbody2D myRigidbody;
    private float horizontalSpeed = 0;
    public float speedMultiplier = 600;
    bool jump = false;
    public float jumpForce = 30;

    public Transform nonSkeletonParts;


    //animations
    private SkeletonAnimation skelAnim;
    [SpineAnimation] public string normal_idle;
    [SpineAnimation] public string normal_walk;
    [SpineAnimation] public string normal_hide;
    [SpineAnimation] public string normal_fall;
    public enum PlayerAnimStates { idle, walking, running, falling, hidding, climbing }
    public PlayerAnimStates currentAnimState = PlayerAnimStates.idle;
    private PlayerAnimStates previousAnimState = PlayerAnimStates.idle;
    private bool basicLoopingAnimsDisabled = false;

    [SpineAnimation] public string slash_anim;
    [SpineAnimation] public string climbePrep_anim;
    [SpineAnimation] public string climbe_anim;
    [SpineAnimation] public string jump_to_fight;

    //reference needed to read current player state
    private Player me;

    //air control
    public bool isGrounded = false;
    public LayerMask groundMask;
    public Transform feet;
    public float feetRadius;

    //for the switch
    private PillManager pm;



    private void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();

        skelAnim = GetComponent<SkeletonAnimation>();

        me = GetComponent<Player>();
        pm = GetComponent<PillManager>();

        current_idle = normal_idle;
        current_walk = normal_walk;
        current_hide = normal_hide;
    }


    bool isJumping = false;
    float jumpTimeCounter = 0;
    public float maxJumpTime = 0;

    private void Update()
    {
        //movement
        if (me.currentState == Player.State.normal)// && isGrounded)
        {
            horizontalSpeed = Input.GetAxis("Horizontal") * speedMultiplier;
        }

        if (me.currentState == Player.State.normal)
        {
            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                isJumping = true;
                jumpTimeCounter = maxJumpTime;
                myRigidbody.velocity = Vector2.up * jumpForce;

            }
            if (Input.GetButton("Jump") && isJumping)
            {
                if (jumpTimeCounter > 0)
                {
                    myRigidbody.velocity = Vector2.up * jumpForce;
                    jumpTimeCounter -= Time.deltaTime;
                }
                else
                {
                    isJumping = false;
                }
            }
            if (Input.GetButtonUp("Jump"))
            {
                isJumping = false;
            }
        }




        //animation
        if (previousAnimState != currentAnimState)
        {
            if(!basicLoopingAnimsDisabled)
                PlayNewStableAnimation();
        }
        previousAnimState = currentAnimState;
    }

    private void PlayNewStableAnimation()
    {
        switch (currentAnimState)
        {
            case PlayerAnimStates.idle: skelAnim.AnimationState.SetAnimation(0, current_idle, true);break;
            case PlayerAnimStates.walking: skelAnim.AnimationState.SetAnimation(0, current_walk, true);break;
            case PlayerAnimStates.falling: skelAnim.AnimationState.SetAnimation(0, normal_fall, true);break;
            case PlayerAnimStates.climbing: skelAnim.AnimationState.SetAnimation(0, climbe_anim, true);break;
            //case PlayerAnimStates.hidding: skelAnim.AnimationState.SetAnimation(0, "crouch", true);break;
            default: skelAnim.AnimationState.SetAnimation(0, current_idle, true); break;
        }
       
    }

    /// <summary>
    /// Stop any animation the player is playing, play attact animation and then go back to idle
    /// </summary>
    public void PlayAttackAnim()
    {        
        if (!striking)
        {
            striking = true;
            StopCoroutine(AttackAnim());
            StartCoroutine(AttackAnim());
        }
    }

    private bool striking = false;

    IEnumerator AttackAnim()
    {
       // if (pm.onDrugs)
        //{
            bool error = false;
            try
            {
                skelAnim.AnimationState.SetAnimation(0, slash_anim, false);
            }
            catch
            {
                error = true;
            }
            if (!error)
            {
                basicLoopingAnimsDisabled = true;
                //skelAnim.AnimationState.SetAnimation(0, slash_anim, false);

                yield return new WaitForSeconds(0.65f);

                skelAnim.AnimationState.SetAnimation(0, current_idle, true);
                basicLoopingAnimsDisabled = false;

            }
        //}
        striking = false;
    }

    /// <summary>
    /// Stop any animation the player is playing, play hide animation and then go back to idle
    /// </summary>
    public void PlayHideAnim()
    {
        StopCoroutine(HideAnim());
        StartCoroutine(HideAnim());
    }

    IEnumerator HideAnim()
    {
        basicLoopingAnimsDisabled = true;
        skelAnim.AnimationState.SetAnimation(0,current_hide, false);

        skelAnim.AnimationState.TimeScale = 2f;

        yield return new WaitForSeconds(0.6f);

        skelAnim.AnimationState.TimeScale = 1f;

        skelAnim.AnimationState.SetAnimation(0, current_idle, true);
        currentAnimState = PlayerAnimStates.idle;
        basicLoopingAnimsDisabled = false;
    }

    public void PlayFinishClimbeAnim()
    {
        StopCoroutine(FinishClimbeAnim());
        StartCoroutine(FinishClimbeAnim());
    }

    IEnumerator FinishClimbeAnim()
    {
        basicLoopingAnimsDisabled = true;
        skelAnim.AnimationState.SetAnimation(0, current_hide, false);

        yield return new WaitForSeconds(1.2f);

        skelAnim.AnimationState.SetAnimation(0, current_idle, true);
        currentAnimState = PlayerAnimStates.idle;
        basicLoopingAnimsDisabled = false;
    }

    public void PlayJumpToFightAnim()
    {
        StopCoroutine(JumpToFightAnim());
        StartCoroutine(JumpToFightAnim());
    }

    IEnumerator JumpToFightAnim()
    {
        basicLoopingAnimsDisabled = true;
        skelAnim.AnimationState.SetAnimation(0, jump_to_fight, false);

        yield return new WaitForSeconds(0.3f);

        skelAnim.AnimationState.SetAnimation(0, current_idle, true);
        currentAnimState = PlayerAnimStates.idle;
        basicLoopingAnimsDisabled = false;
    }

    public void StunnPlayer()
    {
        StopCoroutine(StunnAnim());
        StartCoroutine(StunnAnim());
    }


    /// <summary>
    /// Stop any animation the player is playing, set players state to "stunned" (cant move for 1s), play stunn animation and then go back to idle
    /// </summary>
    IEnumerator StunnAnim()
    {
        me.currentState = Player.State.stunned;
        basicLoopingAnimsDisabled = true;
        Stop();

        ////////////skelAnim.AnimationState.SetAnimation(0, "idle-from fall", false);

        yield return new WaitForSeconds(1f);

        skelAnim.AnimationState.SetAnimation(0, current_idle, true);

        if (me.currentState == Player.State.stunned)
        {
            me.currentState = Player.State.normal;
        }
        basicLoopingAnimsDisabled = false;
    }


    private bool preping = false;
    public void PlayPrepClimbeAnim()
    {
        if (!preping)
        {
            preping = true;
            StopCoroutine(PrepClimbeAnim());
            StartCoroutine(PrepClimbeAnim());
        }
    }
    IEnumerator PrepClimbeAnim()
    {
        basicLoopingAnimsDisabled = true;
        skelAnim.AnimationState.SetAnimation(0, climbePrep_anim, false);

        yield return new WaitForSeconds(0.56f);

        skelAnim.AnimationState.SetAnimation(0, climbe_anim, true);
        currentAnimState = PlayerAnimStates.climbing;
        basicLoopingAnimsDisabled = false;

        preping = false;
    }


    private bool dying = false;
    [SpineAnimation] public string die;
    public void PlayDieAnim()
    {
        if (!dying)
        {
            dying = true;
            StopCoroutine(DieAnim());
            StartCoroutine(DieAnim());
        }
    }
    IEnumerator DieAnim()
    {
        basicLoopingAnimsDisabled = true;
        skelAnim.AnimationState.SetAnimation(0, die, false);

        yield return new WaitForSeconds(60f);

    }

    private float timer = 0;
    public float inAirSpeedMultiplier = 1.3f;
    private void FixedUpdate()
    {
        //move
        if (pm.onDrugs)
        {
            Move(horizontalSpeed * Time.fixedDeltaTime*2,jump);
        }
        else
        {
            if (me.currentState == Player.State.normal)// && isGrounded )//&& Mathf.Abs(horizontalSpeed) > 0f)
            {
                if (!isGrounded) horizontalSpeed *= inAirSpeedMultiplier;
                Move(horizontalSpeed * Time.fixedDeltaTime,jump);
                ///////////////jump = false;
                /*
                if(timer>0.1f&&timer<0.2f)
                {
                    me.walkSounds.Play();
                }

                if (timer > 0.2f && timer < 0.55f)//this part of walk animation is actual movement (jumps)//bylo to 0.2 a 0.6
                {
                    Move(horizontalSpeed * Time.fixedDeltaTime);
                }
                else if (timer < 0.833f)//other parts of walk animation are the pauses between jumps//bylo tu 0.833 pro newPlayer bill
                {
                    Move(horizontalSpeed * 0.1f * Time.fixedDeltaTime);
                }
                else
                {
                    timer = 0;
                    currentAnimState = PlayerAnimStates.idle;
                }
                timer += Time.fixedDeltaTime;*/
            }
            else
            {
                timer = 0;
                if(me.currentState!=Player.State.climbing)
                    currentAnimState = PlayerAnimStates.idle;
            }
        }

        //if not climbing, check if the character stands of the ground (if not, disable movement)
        if (me.currentState != Player.State.climbing)
        {
            bool wasGrounded = isGrounded;
            Collider2D[] colls = Physics2D.OverlapCircleAll(feet.position, feetRadius, groundMask);
            if (colls.Length > 0)
            {
                isGrounded = true;
            }
            else
            {
                if (wasGrounded)
                {
                    isGrounded = false;
                    currentAnimState = PlayerAnimStates.falling;
                }
            }
        }
    }

    private void Move(float speed, bool jump)
    {
        //move
        myRigidbody.velocity = new Vector2(speed, myRigidbody.velocity.y);

        //flip the character if facing the other direction
        if ((speed > 0 && !facingRight) || (speed < 0 && facingRight))
        {
            Flip();
        }

        //animation
        if (Mathf.Abs(myRigidbody.GetRelativePointVelocity(Vector2.zero).x) > 0.00f && isGrounded)
        {
            currentAnimState = PlayerAnimStates.walking;
        }
        else
        {
            if (isGrounded)
                currentAnimState = PlayerAnimStates.idle;
            else
                currentAnimState = PlayerAnimStates.falling;

        }

        /* //jumping
         if (isGrounded && jump)
         {
             // Add a vertical force to the player.
             isGrounded = false;
             if (biggerJump)
             {
                 myRigidbody.AddForce(Vector2.up * biggerJumpForce, ForceMode2D.Impulse);
                 biggerJump = false;
             }
             else
             {
                 myRigidbody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
             }
         }*/
    }

    public void Flip()
    {
        skelAnim.Skeleton.FlipX = facingRight;

        // switch the way the player is labelled as facing
        facingRight = !facingRight;

        //flip non-skeleton parts of player
        Vector3 newSize = nonSkeletonParts.localScale;
        newSize.x *= -1;
        nonSkeletonParts.localScale = newSize;
        
    }



    /// <summary>
    /// sets he player's velocity to 0 (but this does not disable reading input, so he can start moing again)
    /// </summary>
    public void Stop()
    {
        horizontalSpeed = 0;
        myRigidbody.velocity = Vector2.zero;
        currentAnimState = PlayerAnimStates.idle;
    }



    //spine switching...........................
    public SkeletonDataAsset normalSkeletonData;
    public SkeletonDataAsset crazySkeletonData;
    [SpineAnimation] public string crazy_idle;
    [SpineAnimation] public string crazy_walk;
    [SpineAnimation] public string crazy_hide;

    private string current_idle;
    private string current_walk;
    private string current_hide;

    public Collider2D myCollider;
    float normalFeetPos = -0.29f;
    float crazyFeetPos = -2.10f;
    float normalColliderOffset = 3.27f;
    float crazyColliderOffset = 1.27f;

    public override void Switch(bool crazy)
    {
        //the switch
        if (crazy)
        {
            skelAnim.ClearState();
            skelAnim.skeletonDataAsset = crazySkeletonData;
            skelAnim.Initialize(true);
            current_idle = crazy_idle;
            current_walk = crazy_walk;
            current_hide = crazy_hide;
            pm.GetComponent<PlayerActions>().RefreshSkeleton(true);//PlayerActions script also uses the skeleton (to get the sleeve bone)


            //the whole player is too low
            Vector3 newPos = this.transform.position;
            newPos.y += 3;
            this.transform.position = newPos;

            //the feet is too low
            newPos = feet.transform.localPosition;
            newPos.y = crazyFeetPos;
            feet.transform.localPosition = newPos;

            //and the collider
            var newColl = myCollider.offset;
            newColl.y = crazyColliderOffset;
            myCollider.offset = newColl;
        }
        else
        {
            skelAnim.ClearState();
            skelAnim.skeletonDataAsset = normalSkeletonData;
            skelAnim.Initialize(true);
            current_idle = normal_idle;
            current_walk = normal_walk;
            current_hide = normal_hide;
            pm.GetComponent<PlayerActions>().RefreshSkeleton(false);

            Vector3 newPos = this.transform.position;
            newPos.y += 0.5f;
            this.transform.position = newPos;

            //the feet is too low
            newPos = feet.transform.localPosition;
            newPos.y = normalFeetPos;
            feet.transform.localPosition = newPos;

            //and the collider
            var newColl = myCollider.offset;
            newColl.y =normalColliderOffset;
            myCollider.offset = newColl;
        }


        //make things stable
        skelAnim.GetComponent<MeshRenderer>().sortingOrder = SortingLayer.GetLayerValueFromName("player");
        //flip the character if facing the other direction
        if (facingRight==false)
        {
            //flip skeleton
            skelAnim.Skeleton.FlipX = facingRight;
        }
        //start the walk-jump animation and start it from time zero to make it synchronized
        PlayNewStableAnimation();
        timer = 0;


    }
}
