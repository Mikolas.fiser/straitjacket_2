﻿using Spine;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//checks for collisions with hidding spots, climbing walls, look down spots, breakables and enemy rear colliders
//read input for hide/break/climbe/stab and also performs these actions (usually in form of a coroutine)

public class PlayerActions : MonoBehaviour
{

    private float ClimbeSpeedTesting =15f;

    //references...
    private Rigidbody2D myRigidbody;
    private Player me;

    //breaking things
    public LayerMask breakables;
    private BreakableObject currentBreakable;

    //hidding
    private Collider2D currentHiddingSpot;
    public LayerMask hiddeables;
    private HiddingSpot hiddenIn;

    //climbing
    private ClimbingWall currentWall;
    public LayerMask climbeables;
    private LineRenderer sleeveRenderer;
    private Bone sleeveBone;
    private SkeletonAnimation skelAnim;

    //stab attack
    public LayerMask enemyRear;
    private EnemyRear currentEnemy;

    //look down
    public LayerMask lookDownSpots;
    private CameraFollow2 camFollow;
    private LokkDownSpot currentLookDownSpot;

    //soundEffects
    public AudioClip hideBush;
    public AudioClip hideWardrobe;

    public void RefreshSkeleton(bool crazy)
    {
        skelAnim = GetComponent<SkeletonAnimation>();
        if (!crazy)
        {
            sleeveBone = skelAnim.skeleton.FindBone("sleeve");
        }
    }


    private void Start()
    {
        camFollow = Camera.main.GetComponent<CameraFollow2>();
        myRigidbody = GetComponent<Rigidbody2D>();
        me = GetComponent<Player>();
        Debug.Log(me);
        sleeveRenderer = GetComponent<LineRenderer>();

        skelAnim = GetComponent<SkeletonAnimation>();
        sleeveBone = skelAnim.skeleton.FindBone("sleeve");
    }

    public void StopHidding()
    {
        StopAllCoroutines();
        hiddenIn.outlineScript.outlineSize = 5;
        if (hiddenIn.isBush)
        {
            me.otherSounds.clip = hideBush;
            me.otherSounds.Play();
        }
        else
        {
            me.otherSounds.clip = hideWardrobe;
            me.otherSounds.Play();
        }
        Vector3 newPos = hiddenIn.transform.position;
        newPos.z = 0;// -2.25f;
        this.transform.position = newPos;
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        me.currentState = Player.State.normal;
        hiddenIn.SetSpriteToNormal();
        ///////////me.movement.PlayHideAnim();
        hiddenIn = null;

    }

    private void Update()
    {
        if (me.currentState == Player.State.hidden)
        {
            //stop hidding
            if (Input.GetKeyDown(KeyCode.E))
            {
                StopHidding();
            }
        }


        if (me.currentState == Player.State.normal)
        {
            //start hidding 
            if (currentHiddingSpot != null && Input.GetKeyDown(KeyCode.E) && (!me.pillManagment.onDrugs))
            {
                me.currentState = Player.State.hidden;
                hiddenIn = currentHiddingSpot.GetComponent<HiddingSpot>();
                StopAllCoroutines();
                StartCoroutine(StartHide());
            }
            else
            {
                //stabbing
                if (currentEnemy != null && Input.GetKeyDown(KeyCode.E) && (me.pillManagment.onDrugs))
                {
                    me.currentState = Player.State.stabbing;
                    StartCoroutine(StabKill());
                }
                else
                {
                    //climbing 
                    if (currentWall != null && Input.GetKeyDown(KeyCode.W) && (!me.pillManagment.onDrugs) )
                    {
                        me.currentState = Player.State.climbing;
                        me.movement.Stop();
                        StartCoroutine(ClimbeUp());
                        currentWall.climbSign.SetActive(false);
                    }
                    else
                    {
                        //breaking stuff
                        if (currentBreakable != null && Input.GetKeyDown(KeyCode.E) && (me.pillManagment.onDrugs))
                        {
                            currentBreakable.BreakAChunk();
                            me.movement.PlayAttackAnim();
                        }
                    }
                }
            }
        }

    }

    IEnumerator StartHide()
    {
        hiddenIn.animator.SetTrigger("Shake");
        if (hiddenIn.isBush)
        {
            me.otherSounds.clip = hideBush;
            me.otherSounds.Play();
        }
        else
        {
            me.otherSounds.clip = hideWardrobe;
            me.otherSounds.Play();
        }

        //stops player & sets his state to "hidden"
        me.currentState = Player.State.hidden;
        me.movement.Stop();
        //////////////////////////////////////// hiddenIn.hiddingSign.SetActive(false);
        hiddenIn.outlineScript.outlineSize = 0;
        //TODO: fix animation playing..
        //plays animation
        ////////////////////////me.movement.PlayHideAnim();
        ////////////////////yield return new WaitForSeconds(0.15f);// (0.4f);
        //disables player renderer
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        yield return new WaitForSeconds(0.1f);
        //changes sprite on the hidding spot
        hiddenIn.SetSpriteToHidden();
    }


    IEnumerator StabKill()
    {
        //freeze player and enemy
        me.currentState = Player.State.stabbing;
        me.movement.Stop();        
        currentEnemy.me.Freeze();

        //flip player if facing wrong direction
        float enemyX = currentEnemy.me.gameObject.transform.position.x;
        float playerX = transform.position.x;
        if ((enemyX < playerX && me.movement.facingRight == true) || (enemyX > playerX && me.movement.facingRight == false))
        {
            me.movement.Flip();
        }

        //play attack animation
        me.movement.PlayAttackAnim();
        //wait for kill animation to play
        yield return new WaitForSeconds(0.4f);

        //destroy enemy
        currentEnemy.me.DieAnim();
        yield return new WaitForSeconds(0.66f);
        ////////////Destroy(currentEnemy.me.gameObject);
        Manager.deactivated.Add(currentEnemy.me.gameObject);
        currentEnemy.me.gameObject.SetActive(false);

        //set state back to normal
        if (me.currentState == Player.State.stabbing)
        {
            me.currentState = Player.State.normal;
        }
    }

    [SpineAttachment] public string att;
    [SpineAttachment] public string att2;
    [SpineAttachment] public string att3;
    [SpineAttachment] public string att4;
    [SpineSlot] public string slo4;
    [SpineSlot] public string slo2;
    [SpineSlot] public string slo3;
    [SpineSlot] public string slo;

    IEnumerator ClimbeUp()
    {
        ClimbingWall theWall = currentWall;

        //flip player is facing wrond direction
        if ((me.movement.facingRight && theWall.end.transform.position.x - transform.position.x < 0) || ((!me.movement.facingRight) && theWall.end.transform.position.x - transform.position.x > 0))
        {
            me.movement.Flip();
        }

        yield return new WaitForSeconds(0.1f);

        me.movement.PlayPrepClimbeAnim();
        yield return new WaitForSeconds(0.567f);

        //disable player controls and gravity
        me.currentState = Player.State.climbing;
        myRigidbody.isKinematic = true;
        
        me.movement.Stop();
        me.movement.currentAnimState = PlayerMovement.PlayerAnimStates.climbing;

        //sleeve placeholder animation
        this.GetComponent<SkeletonAnimation>().skeleton.SetAttachment(slo4, null);//delete the sleeve
        this.GetComponent<SkeletonAnimation>().skeleton.SetAttachment(slo2, null);//delete the sleeve
        this.GetComponent<SkeletonAnimation>().skeleton.SetAttachment(slo3, null);//delete the sleeve
        this.GetComponent<SkeletonAnimation>().skeleton.SetAttachment(slo, null);//delete the sleeve
        sleeveRenderer.enabled = true;//replace the sleeve with a line renderer
        sleeveRenderer.SetPositions(new Vector3[] { sleeveBone.GetWorldPosition(skelAnim.transform), theWall.railPoint.position });//set position of the line renderer

        //move up until player reaches "top" position
        bool finishing = false;
        while (Mathf.Abs(transform.position.y - theWall.top.position.y) > 0.005f)
        {
            sleeveRenderer.SetPositions(new Vector3[] { sleeveBone.GetWorldPosition(skelAnim.transform), theWall.railPoint.position });
            transform.position = Vector3.MoveTowards(transform.position, theWall.top.position, ClimbeSpeedTesting * Time.deltaTime);
            yield return null;
            if ((!finishing)&&Mathf.Abs(transform.position.y - theWall.top.position.y) < 5f)
            {
                me.movement.PlayFinishClimbeAnim();
                finishing = true;
            }

        }

        //replace the line renderer back with the sleeve
        sleeveRenderer.enabled = false;
        this.GetComponent<SkeletonAnimation>().skeleton.SetAttachment(slo4, att);
        this.GetComponent<SkeletonAnimation>().skeleton.SetAttachment(slo2, att2);
        this.GetComponent<SkeletonAnimation>().skeleton.SetAttachment(slo3, att3);
        this.GetComponent<SkeletonAnimation>().skeleton.SetAttachment(slo, att4);

        //move right until player reaches "end" position
        while (Mathf.Abs(transform.position.x - theWall.end.position.x) > 0.005f)
        {
            transform.position = Vector3.MoveTowards(transform.position, theWall.end.position, ClimbeSpeedTesting * Time.deltaTime);
            yield return null;
        }


        //enable controls and gravity again
        if (me.currentState == Player.State.climbing)
        {
            me.currentState = Player.State.normal;
        }
        myRigidbody.isKinematic = false;
    }


    private void FixedUpdate()
    {
        Collider2D coll;


        //look for a hidding spot
        if (me.currentState == Player.State.normal && (!me.pillManagment.onDrugs))
        {
            coll = GetCurrentCollision(hiddeables);
            if (coll != null)
            {
                //if player is touching a hidding spot appera the hide button
                currentHiddingSpot = coll;
                ///////////////////////////////////////////currentHiddingSpot.GetComponent<HiddingSpot>().hiddingSign.SetActive(true);

            }
            else
            {
                //else say there is no hidding spot available (and cancel the hide button if there was one before)
                if (currentHiddingSpot != null)
                {
                    //////////////////////////currentHiddingSpot.GetComponent<HiddingSpot>().hiddingSign.SetActive(false);
                }
                currentHiddingSpot = null;
            }
        }
        else
        {
            //just in case cancel any leftover highlight...
            if (currentHiddingSpot != null)
            {
               ////////////////////////// currentHiddingSpot.GetComponent<HiddingSpot>().hiddingSign.SetActive(false);
            }
            currentHiddingSpot = null;
        }


        //look for a breakable
        coll = GetCurrentCollision(breakables);
        if (coll != null)
        {
            if (coll.gameObject.GetComponent<BreakableObject>() != null)
            {
                currentBreakable = coll.gameObject.GetComponent<BreakableObject>();
            }
        }
        else
        {
            if (currentBreakable != null)
            {
                currentBreakable.Leave();
            }
            currentBreakable = null;
        }

        //look for a climbeable
        if (me.currentState == Player.State.normal && (!me.pillManagment.onDrugs))
        {
            coll = GetCurrentCollision(climbeables);
            if (coll != null)
            {

                ClimbingWall cw = coll.gameObject.GetComponent<ClimbingWall>();
                //also check if top collider of the wall is not blocked by a breakable
                Collider2D[] results = new Collider2D[2];
                ContactFilter2D cf = new ContactFilter2D();
                cf.useTriggers = true;
                cf.SetLayerMask(breakables);
                int count = cw.topc.OverlapCollider(cf, results);
                if (count==0)
                {
                    currentWall = cw;
                    currentWall.climbSign.SetActive(true);
                }
            }
            else
            {
                if (currentWall != null)
                {
                    currentWall.climbSign.SetActive(false);
                }
                currentWall = null;
            }
        }
        else//just in case cancel any leftover highlight...
        {
            if (currentWall != null)
            {
                currentWall.climbSign.SetActive(false);
            }
            currentWall = null;
        }


        //look for enemy that you are able to stab from behind
        if (me.currentState == Player.State.normal && me.pillManagment.onDrugs)
        {
            coll = GetCurrentCollision(enemyRear);
            if (coll != null)
            {
                currentEnemy = coll.gameObject.GetComponent<EnemyRear>();
                currentEnemy.icon.SetActive(true);

            }
            else
            {
                if (currentEnemy != null)
                {
                    currentEnemy.icon.SetActive(false);
                }
                currentEnemy = null;

            }
        }
        else
        {//just in case cancel any leftover highlight...
            if (currentEnemy != null)
            {
                currentEnemy.icon.SetActive(false);
            }
        }


        //look for a look down spot
        coll = GetCurrentCollision(lookDownSpots);
        if (coll != null)
        {
            camFollow.canLookDownNow = true;
            currentLookDownSpot = coll.GetComponent<LokkDownSpot>() ;                                                             
            currentLookDownSpot.lookDownSign.SetActive(true);
        }
        else
        {
           //else say there is no look down spot available (and cancel button if there was one before)
           if (currentLookDownSpot != null)                                                           
           {       
               currentLookDownSpot.lookDownSign.SetActive(false);
           }
           currentLookDownSpot = null;
           camFollow.canLookDownNow = false;
        }

    }


    private Collider2D GetCurrentCollision(LayerMask mask)
    {
        Collider2D[] colls = Physics2D.OverlapCircleAll(transform.position, 1f, mask);
        if (colls.Length > 0)
            return colls[0];
        else
            return null;
    }


}
