﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//this will be the corridor leading to next level

public class EndLevel : MonoBehaviour
{
    public int lvl;
    public CameraFollow2 camera;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (lvl == 1)
        {
            Saving.level = 2;
            Saving.position = new Vector3(564.67f, -125.86f, -2.25f);
            StartCoroutine(Fade());
        }
        else
        {
            if (lvl == 2)
            {
                Saving.level = 3;
                Saving.position = new Vector3(572f, -125.5f, -2f);
                StartCoroutine(Fade());
            }
        }
    }

    IEnumerator Fade()
    {
        camera.FadeOut();
        yield return new WaitForSeconds(1f);
        if(lvl==1)
            SceneManager.LoadScene("level2");
        else if(lvl==2)
            SceneManager.LoadScene("BossFight");
    }
}
