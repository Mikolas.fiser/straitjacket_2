﻿using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossView : MonoBehaviour {

    private Player player;
    public ParticleSystem fire;
    public ParticleSystem injections;
    public Boss me;

    public Animator viewStuffAnimator;
    public SkeletonAnimation dragonHead;
    private string idle;
    private string shout;


    void Start()
    {
        player = FindObjectOfType<Player>();
    }

    private bool breathingFire = false;
	void OnTriggerStay2D(Collider2D other)
    {
        if ((!breathingFire)&&other.CompareTag("Player") && (player.currentState!=Player.State.hidden))
        {
            breathingFire = true;
            StartCoroutine(BossDeadlyAttack());
        }
    }

    IEnumerator BossDeadlyAttack()
    {
        GetComponent<Renderer>().enabled = false;
        me.frozen = true;
        viewStuffAnimator.speed = 0;
        player.movement.Stop();
        player.currentState = Player.State.stunned;

        dragonHead.AnimationState.SetAnimation(0, "magic FIREBREATH", true);

        if (player.pillManagment.onDrugs)
        {
            me.soundFire.Play();
            ///////me.soundKitten.Play();
            fire.Play();
            fire.playbackSpeed = 10;
            me.soundPlayerDeath.Play();
        }
        else
        {
            injections.Play();
            injections.playbackSpeed = 5;
            player.movement.PlayDieAnim();
        }
        
        yield return new WaitForSeconds(0.3f);
        fire.playbackSpeed = 1;
        injections.playbackSpeed = 1;
        yield return new WaitForSeconds(3f);
        while (me.soundFire.volume > 0.1f)
        {
            me.soundFire.volume -= Time.deltaTime * 2;
            me.soundKitten.volume -= Time.deltaTime * 2;
            yield return null;
        }
        player.KillPlayer();
        //sound fadeout

        me.frozen = false;
        GetComponent<Renderer>().enabled = true;
        viewStuffAnimator.speed = 1;

        dragonHead.AnimationState.SetAnimation(0, "magic MIDDLE", true);

        me.soundFire.Stop();
        me.soundKitten.Stop();
    }
}
