﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//contains public methods KillPlayer() and StartFighting(enemy)
//holds current state of the player (normal, hidden, climbing, fighting, stabbing, stunned)
//holds references to other parts of player

public class Player : MonoBehaviour
{
    //references to other parts of the player
    public PlayerActions actions;
    public PlayerMovement movement;
    public PillManager pillManagment;

    //fighting
    private int numberOfFights = 0;
    public AudioSource fightSounds;
    public GameObject fightCloud;

    //the state of the player
    public State currentState = State.normal;
    public enum State { normal, hidden, climbing, fighting, stabbing, stunned };

    //respawning
    public Vector3 respawnPoint = new Vector3(-465, 13,-1);

    private List<Enemy> enemies = new List<Enemy>();

    public void Start()
    {
        enemies.AddRange(FindObjectsOfType<Enemy>());
    }

    //soundEffects
    public AudioSource walkSounds;
    public AudioSource otherSounds;

    /// <summary>
    /// stops all the player actions and resets him to normal state, moves him to respawn position and activates all deactivated object (enemies, breakables...)
    /// </summary>
    public void KillPlayer()
    {
        if (!dead)
        {
            dead = true;
            StartCoroutine(Death());
        }
    }
    bool dead = false;
    IEnumerator Death()
    {
        movement.PlayDieAnim();
        movement.enabled = false;
        actions.enabled = false;
        
        yield return new WaitForSeconds(4f);

        if (Saving.level == 0)
            SceneManager.LoadScene("TestingScene5");
        else if (Saving.level == 1)
            SceneManager.LoadScene("level1");
        else if (Saving.level == 2)
            SceneManager.LoadScene("level2");
        else
            SceneManager.LoadScene("BossFight");
    }


    /// <summary>
    /// Starts a fight with given enemy
    /// </summary>
    /// <param name="enemy"></param>
    public void StartFighting(Enemy enemy)
    {
        currentState = Player.State.fighting;
        numberOfFights++;//to remmember how many fights are happening at once
        StartCoroutine(Fight(enemy));
    }


    IEnumerator Fight(Enemy enemy)
    {
        //freez both fighters
        bool won = false;
        enemy.Freeze();
        currentState = Player.State.fighting;
        movement.Stop();

        //make them face each other...
        float enemyX = enemy.gameObject.transform.position.x;
        float playerX = transform.position.x;
        //flip player
        if ((enemyX < playerX && movement.facingRight == true) || (enemyX > playerX && movement.facingRight == false))
        {
            movement.Flip();
        }
        //flip enemy
        if ((enemyX < playerX && enemy.facingRight == false) || (enemyX > playerX && enemy.facingRight == true))
        {
            enemy.Flip();
        }

        //make them approach each other...
        Vector3 newVect;
        enemy.StartApproachingAnim();
        movement.PlayJumpToFightAnim();
        while (Mathf.Abs(enemy.gameObject.transform.position.x - playerX)>4f)
        {
            newVect = Vector3.MoveTowards(enemy.transform.position, transform.position, 7 * Time.deltaTime);
            enemy.transform.position = new Vector3(newVect.x, enemy.transform.position.y, enemy.transform.position.z);
            yield return null;
        }

        //when they are close to each other enemy starts to fight
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        enemy.myFrontView.GetComponent<SpriteRenderer>().enabled = false;
        enemy.GetComponent<MeshRenderer>().enabled = false;
        fightSounds.Play();
        fightCloud.SetActive(true);

        enemy.StartFightingAnim();

        //start counting 5 seconds
        float timer = 5f;
        enemy.progressBar.SetActive(true);
        enemy.filling.fillAmount = 0;

        //display the first key to press
        int nextKeyToPres = 0;
        enemy.barText.text = enemy.combatKeys[0].ToString();

        //if the player presses the correct buttons during this time he defeats the enemy
        //otherwise he dies
        while (timer > 0)
        {
            yield return null;
            //update timer
            timer -= Time.deltaTime;
            //update progress bar
            enemy.filling.fillAmount += 1.0f / 5 * Time.deltaTime;


            if (Input.GetKeyDown(enemy.combatKeys[nextKeyToPres]))
            {
                nextKeyToPres++;
                if (nextKeyToPres >= enemy.combatKeys.Count)//if managed to press the whole sequence of buttons the player kills the enemy
                {
                    won = true;
                    movement.PlayAttackAnim();
                    yield return new WaitForSeconds(0.4f);
                    break;
                }
                else
                {
                    //if there are still some missing display the next key to press
                    enemy.barText.text = enemy.combatKeys[nextKeyToPres].ToString();
                    movement.PlayAttackAnim();
                }
            }
            //if pill ran out during the combat the player lost
            if(!pillManagment.onDrugs)
            {
                break;
            }
        }

        //fight is over
        //enable the enemy visuals
        enemy.GetComponent<MeshRenderer>().enabled = true;
        enemy.myFrontView.GetComponent<SpriteRenderer>().enabled = true;

        //if it was the only fight also enable the player visuals
        if (numberOfFights <= 1)
        {
            fightSounds.Stop();
            fightCloud.SetActive(false);
            gameObject.GetComponent<MeshRenderer>().enabled = true;
        }

        enemy.progressBar.SetActive(false);
        numberOfFights--;


        //unfreez the player if he is not in any other fight
        if (numberOfFights <= 0)
        {
            currentState = Player.State.normal;
        }



        if (!won)
        {
            //the player was killed
            KillPlayer();
            //also unfreeze the enemy
            enemy.Unfreeze();
            enemy.myFrontView.Reactivate();

        }
        else
        {
            //play death anim
            enemy.DieAnim();
            yield return new WaitForSeconds(0.66f);
            //also unfreeze the enemy
            enemy.Unfreeze();
            //kill the enemy
            enemy.myFrontView.Reactivate();
            //the enemy was killed
            /////////////////Destroy(enemy.gameObject);
            Manager.deactivated.Add(enemy.gameObject);
            enemy.gameObject.SetActive(false);
        }

       

    }
}
