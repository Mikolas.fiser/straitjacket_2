﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class EndScene : MonoBehaviour {

    public VideoPlayer endVideo;

    void Awake()
    {
        StartCoroutine(PlayIt());
    }

    IEnumerator PlayIt()
    {
        endVideo.Prepare();
        while (!endVideo.isPrepared)
            yield return null;
        endVideo.Play();
        yield return new WaitWhile(delegate { return endVideo.isPlaying == true; });
        SceneManager.LoadScene("IntroScene");
    }
}
