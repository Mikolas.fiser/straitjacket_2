﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

//the breakable obstacle object (canbe broken in crazy world)
//it is supposed to be a horizontal door/pinata, must have a 2D collider and be on layer "breakables"
//has public methods BreakOneChunk and Leave whichinfluence the breaking progress bar

public class BreakableObject : MonoBehaviour
{
    //references to progress bar of this breakable
    public GameObject progressBarObject;
    public Image progressBarFill;

    //parameters of this breakable
    public float oneChunkSize = 0.1f;//how quickly it breaks
    public ParticleSystem pinataEffect;//on break effect
    public Animator pinataShake;


    private bool coolingDown;
    private bool broken;

    private Transform player;

    public Transform theDoorWall;
    private Manager mngr;


    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        progressBarFill.fillAmount = 0;
        coolingDown = false;
        broken = false;
        mngr = GameObject.Find("Manager").GetComponent<Manager>();

    }

    private void OnEnable()
    {
        coolingDown = false;
        broken = false;
        progressBarFill.fillAmount = 0;
        progressBarObject.SetActive(false);
    }

    /// <summary>
    /// Increases the fill amount of progress bar by chunkSize of this breakable & checks if full.
    /// </summary>
    public void BreakAChunk()
    {
        
        if (!broken)
        {
            //increase progress bar
            coolingDown = true;
            progressBarObject.SetActive(true);
            progressBarFill.fillAmount += oneChunkSize;

           //play shake anim
           if(pinataShake!=null)
               pinataShake.SetTrigger("shake");
            if(!pinataEffect.isEmitting)
                pinataEffect.Play();

            //position the bar to a good location
            Vector3 barPos = player.transform.position;
            barPos.y += 4;
            progressBarObject.transform.position = barPos;

            //check if it is full
            if (progressBarFill.fillAmount >= 1)
            {
                //break this breakable
                broken = true;
                progressBarObject.SetActive(false);
                StartCoroutine(Breakdown());
                if(theDoorWall!=null)
                    mngr.walls.Remove(theDoorWall);
            }
        }
    }

    /// <summary>
    /// Cancels the process of breaking.
    /// </summary>
    public void Leave()
    {
        progressBarFill.fillAmount = 0;
        progressBarObject.SetActive(false);
    }


    void Update()
    {
        //decrease the fill amount of progress bar
        if (coolingDown == true)
        {
            progressBarFill.fillAmount -= 1.0f / 15 * Time.deltaTime;
        }
        //if decreased to zero disable the progress bar competely
        if (progressBarFill.fillAmount <= 0)
        {
            coolingDown = false;
            progressBarObject.SetActive(false);
        }
    }


    //play destroy effect and destroy this breakable
    IEnumerator Breakdown()
    {
        pinataEffect.Play();
        yield return new WaitForSeconds(0.5f);
        ////////////Destroy(this.gameObject);
        Manager.deactivated.Add(this.gameObject);
        this.gameObject.SetActive(false);
    }

}
