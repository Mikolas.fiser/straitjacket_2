﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMunu : MonoBehaviour {

    public Selectable firstButton;
    public EventSystem e;
    public Button[] buttons = new Button[7];


    public void OnEnable()
    {
        for (int i = 0; i < 7; i++)
        {
            if (Saving.unluckedCheckpoints[i] == false)
            {
                buttons[i].interactable = false;
            }
            else
            {
                buttons[i].interactable = true;
            }
        }


        if (firstButton != null)
        {
            firstButton.Select();
            e.SetSelectedGameObject(firstButton.gameObject);
        }
        Cursor.visible = false;
        firstButton.Select();
    }

    public void Continue()
    {
        ////Cursor.visible = true;
        this.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void Quit()
    {
        ////Debug.Log("Quit");
        Application.Quit();
    }

    public void Press1a()
    {
        Time.timeScale = 1;
        Saving.level = 0;
        Saving.position = new Vector3(115, 172, 0);// new Vector3(16.35f, -2.47f, -2.25f);
        Saving.hasPill = false;
        SceneManager.LoadScene("TestingScene3");
    }

    public void Press1b()
    {
       /* Time.timeScale = 1;
        Saving.level = 1;
        Saving.position = new Vector3(308.78f, -13.29f, -2.25f);
        Saving.hasPill = false;
        SceneManager.LoadScene("level1");*/
    }

    public void Press1c()
    {
       /* Time.timeScale = 1;
        Saving.level = 1;
        Saving.position = new Vector3(626.27f, -71.82f, -2.25f);
        Saving.hasPill = false;
        SceneManager.LoadScene("level1");*/
    }

    public void Press2a()
    {
        /*Time.timeScale = 1;
        Saving.level = 2;
        Saving.position = new Vector3(564.67f, -125.86f, -2.25f);
        Saving.hasPill = false;
        SceneManager.LoadScene("level2");*/
    }

    public void Press2b()
    {
        /*Time.timeScale = 1;
        Saving.level = 2;
        Saving.position = new Vector3(554.2f, -165.8f, -2.25f);
        Saving.hasPill = true;
        SceneManager.LoadScene("level2");*/
    }

    public void Press2c()
    {
        /*Time.timeScale = 1;
        Saving.level = 2;
        Saving.position = new Vector3(775.34f, -165.515f, -2.25f);
        Saving.hasPill = true;
        SceneManager.LoadScene("level2");*/
    }

    public void PressBoss()
    {
        /*Time.timeScale = 1;
        Saving.level = 3;
        Saving.position = new Vector3(571.787f, -125.5f, -2f); 
        Saving.hasPill = false;
        SceneManager.LoadScene("BossFight");*/
    }
}
