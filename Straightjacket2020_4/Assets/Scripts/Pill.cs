﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//the fighting pill object
//must have a 2D collider

public class Pill : MonoBehaviour
{

    private PillManager mngr;
    private bool deleted = false;

    private void Awake()
    {
        mngr = GameObject.FindGameObjectWithTag("Player").GetComponent<PillManager>();
    }

    void OnEnable()
    {
        deleted = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if the player touches the pill it is added to his supply
        if (collision.gameObject.CompareTag("Player"))
        {
            if (!deleted)
            {
                mngr.AddPill();
                //////////////////////////Destroy(gameObject);
                Manager.deactivated.Add(this.gameObject);
                this.gameObject.SetActive(false);

                deleted = true;
            }
        }
    }
}
