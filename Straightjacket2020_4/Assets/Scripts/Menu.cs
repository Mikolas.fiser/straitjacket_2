﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

//the menu and functions for its buttons

public class Menu : MonoBehaviour
{
    public Selectable firstButton;
    public Selectable continueButton;

    public VideoPlayer video;
    public GameObject skipButton;
    private bool canSkip;
    public GameObject theMenu;
    public Image fader;
    private bool starting;

    private AudioSource backgroundMusic;


    public EventSystem e;

    void OnEnable()
    {
        Cursor.visible = false;
        e.SetSelectedGameObject(firstButton.gameObject);
        firstButton.Select();
        
        starting = false;
        canSkip = false;

        backgroundMusic = GetComponent<AudioSource>();
        backgroundMusic.Play();

        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {
            //display Continue button
            continueButton.gameObject.SetActive(true);

            //load the file
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Debug.Log(Application.persistentDataPath );
            SavedData save = (SavedData)bf.Deserialize(file);
            file.Close();

            //fill the static class Saving with the info
            Saving.level = save.level;
            Saving.hasPill = save.hasPill;
            Saving.position = new Vector3(save.posX, save.posY, save.posZ);
            for (int i = 0; i < 7; i++)
            {
                if(i<=save.maxUnlockedIndex)
                {
                    Saving.unluckedCheckpoints[i] = true;
                }
                else
                {
                    Saving.unluckedCheckpoints[i] = false;
                }
            }
        }
    }

    public void ContinueGame()
    {   if (Saving.level == 0)
            SceneManager.LoadScene("TestingScene3");
        else if (Saving.level == 1)
            SceneManager.LoadScene("level1");
        else if (Saving.level == 2)
            SceneManager.LoadScene("level2");
        else if (Saving.level == 3)
            SceneManager.LoadScene("BossFight");
    }


    public void StartNewGame()
    {
            if (!starting)
                StartCoroutine(NewGame());

    }

    IEnumerator NewGame()
    {
        //set Saving to a new game(nothing unlocked etc.) (but do not rewrute any savefile yet, it will happen on the first checkpoint)
        Saving.level = 0;
        Saving.hasPill = false;
        Saving.position = new Vector3(115, 172, 0);// new Vector3(16.35f, -2.47f, -2.25f);
        Saving.unluckedCheckpoints = new bool[7] { true, false, false, false, false, false, false };

        //fade screen to black and fade music volume
        starting = true;
        float alpha = 0;
        video.Prepare();
        while (alpha<1)
        {
            backgroundMusic.volume -= 1 * Time.deltaTime;
            fader.color = new Color(0, 0, 0, alpha);
            alpha += 1 * Time.deltaTime;
            yield return null;
        }
        backgroundMusic.Stop();

        //wait for video to prepare, then play it
        while(!video.isPrepared)
        {
            yield return null;
        }
        yield return null;
        video.Play();
        yield return null;

        //enable skip button
        fader.enabled = false;
        skipButton.gameObject.SetActive(true);
        canSkip = true;
        theMenu.SetActive(false);

        //when the video is over load the first level
        yield return new WaitWhile(delegate{ return video.isPlaying == true; });
        SceneManager.LoadScene("TestingScene3");
    }

    public void Update()
    {
        if(canSkip && Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene("TestingScene3");
        }
    }

    public void Continue()
    {
        this.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void Quit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
