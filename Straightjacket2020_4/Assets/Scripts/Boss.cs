﻿using Spine;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    //movement
    public bool frozen = false;

    //collider logic
    public Collider2D myCollider;
    bool triggered = false;
    private Player player;
    private BossRoom room;
    public Transform startingPoint;
    public Transform pushPoint;
    public BossView myView;
    public ParticleSystem smoke;

    public List<GameObject> lifeIndicators = new List<GameObject>();
    private Stack<GameObject> lives = new Stack<GameObject>();


    //sound effects
    public AudioSource soundFire;
    public AudioSource soundRoar;
    public AudioSource soundKitten;
    public AudioSource soundPlayerDeath;

    void Start()
    {
        player = FindObjectOfType<Player>();
        room = FindObjectOfType<BossRoom>();

        foreach (var life in lifeIndicators)
            lives.Push(life);
    }


    bool enterdNotReady = false;
    void OnTriggerEnter2D(Collider2D other)
    {
        //when player gets close to the boss
        if (other.gameObject.CompareTag("Player"))
        {
            if (!player.pillManagment.onDrugs)
            {
                enterdNotReady = true;
            }
            else
            {
                //strike boss
                if (!triggered)
                {
                    triggered = true;
                    frozen = true;
                    StartCoroutine(BossAttack(false));
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        enterdNotReady = false;
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if(enterdNotReady && player.pillManagment.onDrugs)
        {
            enterdNotReady = false;
            //strike boss
            if (!triggered)
            {
                triggered = true;
                frozen = true;
                StartCoroutine(BossAttack(true));
            }
        }
    }



    IEnumerator BossAttack(bool waitFirst)
    {
        //lock player
        player.movement.Stop();
        player.currentState = Player.State.stunned;

        //disable the killing cone of vision for now
        myView.gameObject.SetActive(false);

        //wait for SWITCH to be completed
        if (waitFirst)
            yield return new WaitForSeconds(1f);

        //player strike
        player.movement.PlayAttackAnim();
        yield return new WaitForSeconds(0.3f);

        //dragon shout
        myView.dragonHead.AnimationState.SetAnimation(0, "magic FIREBREATH", true);


        //deastroy one life
        lives.Pop().SetActive(false);
        soundRoar.Play();

        if (room.phase < 3)
        {
            //push player to start point
            smoke.Play();
            while (Mathf.Abs(player.transform.position.x - pushPoint.position.x) > 0.005f)
            {
                player.transform.position = Vector3.MoveTowards(player.transform.position, pushPoint.position, 30 * Time.deltaTime);
                yield return null;
            }
            //reactivate all the stuff
            myView.dragonHead.AnimationState.SetAnimation(0, "magic MIDDLE", true);
            myView.gameObject.SetActive(true);
            triggered = false;
            frozen = false;
            smoke.Stop();
            player.currentState = Player.State.normal;
        }

        room.PhaseChangeEffect();
        room.phase++;

    }
}
