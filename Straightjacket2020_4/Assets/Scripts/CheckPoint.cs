﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

//must have a 2D trigger collider
//on trigger enter the game is saved (saves the respawn position of the player and if he has a pill available)

public class CheckPoint : MonoBehaviour {

    private bool triggered = false;
    private Player p;
    private PillManager pm;

    public int number;

    void Start()
    {
        p = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        pm = GameObject.FindGameObjectWithTag("Player").GetComponent<PillManager>();
    }

	void OnTriggerEnter2D(Collider2D other)
    {
        if (!triggered)
        {
            
            triggered = true;
            SaveStuff();
            ////////////Destroy(this.gameObject);
            GetComponent<Collider2D>().enabled = false;
        }
    }




    private void SaveStuff()
    {
        //position
        p.respawnPoint = this.transform.position;

        //availability of pills
        if(pm.fightPillsAvailable)
        {
            Manager.hadPillOnLastCheckpoint = true;
        }
        else
        {
            Manager.hadPillOnLastCheckpoint = false;
        }

        //things destroyed before this checkpoint will remain destroyed forever
        Manager.deactivated.Clear();
        
        //if it is an unlockable checkpoint
        if (number != -1)
        {
            //unlock this checkpoint (it could be unlocked already but that is not a problem)
            Saving.unluckedCheckpoints[number] = true;
        }

        //and fill also the rest of the static class Saving
        Saving.hasPill = pm.fightPillsAvailable;
        Saving.position = this.transform.position;
    }
}
