﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//the front cone of view of the enemy
//either kills the player or starts combat with him

public class EnemyView : MonoBehaviour
{

    private Player player;
    [SerializeField] private Enemy me;
    private bool alreadyActivated = false;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!alreadyActivated)
        {
            //when not on drugs all enemies, except for the Patient type, cause instant death
            if (!player.pillManagment.onDrugs)
            {
                if (me.type != Enemy.EnemyTypes.patient)
                {
                    //if the player touched the enemy view area
                    if (collision.gameObject.CompareTag("Player"))
                    {
                        //and if the player was not hidden
                        if (player.currentState != Player.State.hidden)
                        {
                            if (me.type == Enemy.EnemyTypes.walker) //walkers chase player
                            {
                                alreadyActivated = true;
                                me.StartChasingPlayer();
                            }
                            else  //others instantly kill him
                            {
                                //start me.jumpKill coroutine (jump anim, wait, kill) instead of just kill
                                /////player.KillPlayer();
                                me.JumpKill();
                            }
                        }

                    }
                }
            }
            else
            {
                //if the player touched the enemy view area
                if (collision.gameObject.CompareTag("Player"))
                {
                    //and if the player is in normal state or already in a fight and also not in the air
                    if ((player.currentState == Player.State.normal || player.currentState == Player.State.fighting)&& player.movement.isGrounded)
                    {
                        //start a fight with this enemy
                        alreadyActivated = true;
                        player.StartFighting(me);
                    }
                }
            }
        }

    }


    void OnEnable()
    {
        Reactivate();
    }

    public void Reactivate()
    {
        StopAllCoroutines();
        StartCoroutine(Activate());
    }

    IEnumerator Activate()
    {
        yield return new WaitForSeconds(1f);
        alreadyActivated = false;
    }


}
