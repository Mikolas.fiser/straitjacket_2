﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//teleportation just for debugging

public class SpawnSelector : MonoBehaviour {

    public List<Transform> points = new List<Transform>();
    public GameObject panel;
    public Button buttonPrefab;

    private Player p;

    void Start()
    {
        p = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        Button x = Instantiate(buttonPrefab, panel.transform);
        x.GetComponentInChildren<Text>().text = "GoToFirstLevel";
        x.onClick.AddListener(delegate () { GoToFirst(); });

        Button z = Instantiate(buttonPrefab, panel.transform);
        z.GetComponentInChildren<Text>().text = "GoToSecondLevel";
        z.onClick.AddListener(delegate () { GoToSecond(); });

        foreach (Transform item in points)
        {
            Button y = Instantiate(buttonPrefab, panel.transform);
            y.GetComponentInChildren<Text>().text = item.gameObject.name;
            Vector3 pos = item.position;
            y.onClick.AddListener(delegate() { Respawn(pos); });
        }
    }

    void Respawn(Vector3 point)
    {
        p.respawnPoint = point;
        p.KillPlayer();
    }

    public void GoToFirst()
    {
        SceneManager.LoadScene("level1");
    }

    public void GoToSecond()
    {
        SceneManager.LoadScene("level2");
    }
}
