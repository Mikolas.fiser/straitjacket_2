﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//must have a 2D collider, will be EvilTV probably...
//when player is in reach of the collider he is forced to remain in normal world

public class EvilPortrait : Switcher {

    private PillManager pillMngr;
    private PlayerMovement pm;

    public Sprite normalSprite;
    public Sprite crazySprite;
    public SpriteRenderer myRenderer;
    public Animator anim;

    void Start()
    {
        pillMngr = GameObject.FindGameObjectWithTag("Player").GetComponent<PillManager>();
        pm = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
        
    }


    //if the player is in a range given by this collider
    //he is forced to exit the drug world (also is stunned for a second)
	void OnTriggerStay2D(Collider2D collision)
    {
        if(pillMngr.onDrugs && collision.gameObject.CompareTag("Player"))
        {
            pillMngr.ExitDrugs();
            pm.StunnPlayer();
            pillMngr.switchingAllowed = false;
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            pillMngr.switchingAllowed = false;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        pillMngr.switchingAllowed = true;
    }

    public override void Switch(bool crazy)
    {
        if (myRenderer != null)
        {
            if (crazy)
            {
                myRenderer.sprite = crazySprite;
                anim.SetBool("crazy", true);
            }
            else
            {
                anim.SetBool("crazy", false);
                myRenderer.sprite = normalSprite;
            }
        }
    }
}
