﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class BossRoom : MonoBehaviour {

    public CameraFollow2 theCamera;
    public PillManager pillManager;
    public int phase = 1;

    public GameObject enemiesForPhase0;
    public GameObject enemiesForPhase1;
    public GameObject enemiesForPhase2;

    public Enemy e1;
    public Enemy e2;
    public Enemy e3;
    public Enemy e4;
    public Enemy e5;

    public KeyCode magicKey;//to skip to outro scene

    private bool magic = true;
    void Update()
    {
        if(magic && Input.GetKeyDown(magicKey))
        {
            magic = false;
            StartCoroutine(Effect3());
        }
    }

    private bool lightBroken = false;
    public Light entranceLight;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            theCamera.inBossRoom = true;
            if(!lightBroken)
            {
                StartCoroutine(EntranceLightFlicker());
                lightBroken = true;
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            theCamera.inBossRoom = false;
        }
    }

    IEnumerator EntranceLightFlicker()
    {
        for (int i = 0; i < 4; i++)
        {
            entranceLight.intensity = 0;
            yield return new WaitForSeconds(0.2f);
            entranceLight.intensity = 2;
            yield return new WaitForSeconds(0.1f);
        }
        entranceLight.intensity = 0;
    }

    public void PhaseChangeEffect()
    {
        switch (phase)
        {
            case 1:
                StartCoroutine(Effect1());
                break;
            case 2: 
                StartCoroutine(Effect2());
                break;
            case 3: 
                StartCoroutine(Effect3());
                break;
        }
    }

    IEnumerator Effect1()
    {
        enemiesForPhase1.SetActive(true);
        pillManager.AddSwitchable(e1);
        pillManager.AddSwitchable(e2);
        yield return null;
        e1.Switch(true);
        e2.Switch(true);
    }

    IEnumerator Effect2()
    {
        enemiesForPhase2.SetActive(true);
        pillManager.AddSwitchable(e3);
        pillManager.AddSwitchable(e4);
        yield return null;
        e3.Switch(true);
        e4.Switch(true);
        e5.Switch(true);
    }

    IEnumerator Effect3()
    {
        theCamera.FadeOut();
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("EndScene");
    }
}
